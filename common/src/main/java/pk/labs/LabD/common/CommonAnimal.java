/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabD.common;


import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.Logger;
import org.osgi.service.component.ComponentContext;
/**
 *
 * @author Mateusz
 */
public class CommonAnimal implements Animal{

     private String name;
     private String status;
     private String species;
     private Logger logger;
     private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
     
     
     void activate(ComponentContext context)
    {
        name = (String) context.getProperties().get("name");
        species = (String) context.getProperties().get("species");
        logger.log(this, "incoming -" + this.name);
        System.out.println("activate");
    }
    
    void deactivate(ComponentContext context)
    {
        name = (String) context.getProperties().get("name");
        species = (String) context.getProperties().get("species");
        logger.log(this, "outgoing -" + this.name);
        System.out.println("deactivate");
    }
    
    @Override
    public String getSpecies() {
        return species;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }
    
     public void setLogger(Logger logger)
    {
        this.logger=logger;
    }
}
